# summer-excel
![jdk8](https://img.shields.io/badge/JAVA-1.8+-green.svg)
[![release](https://img.shields.io/badge/release-0.5.1-blue)](https://mvnrepository.com/artifact/tk.jfree.summer/summer-excel)

## 介绍
summer-excel是一个基于对象关系的Excel列表数据读写工具。

## 使用示例

### 数据类型

|JavaType|ExcelType|
|:---|:---|
|java.lang.String |CellType.STRING|
|java.lang.Boolean|CellType.BOOLEAN|
|java.lang.Integer|CellType.NUMERIC|
|java.lang.Date   |CellType.NUMERIC|
|java.lang.Enum   |CellType.STRING|
| ...|...|

### 使用 Maven  

在 pom.xml 中添加如下依赖：

```xml
<dependency>
  <groupId>tk.jfree.summer</groupId>
  <artifactId>summer-excel</artifactId>
  <version>0.5.1</version>
</dependency>
```


### 对象关系映射
```java
@Table(sheet = "日志信息", first = 1)
public class Log {
    @Column(name = "A", notes = "账号")
    private String username;
    @Column(name = "B", notes = "时间", format = "yyyy-MM-dd hh:mm:ss", typeHandler = DateTimeTypeHandler.class)
    private java.util.Date gmtCreate;
    @Column(name = "C", notes = "操作")
    private String type;

}
``` 
### Excel读取
``` Java
  ExcelHelper.builder(Log.class).read(new FileInputStream("log.xlsx")).getData().stream().forEach(System.out::println);
``` 
### Excel数据写入
####数据展示
![excel写入数据展示](https://img-blog.csdnimg.cn/20190826014036305.png)
####格式设置
![excel写入数据展示](https://img-blog.csdnimg.cn/20190826014445578.png)

```java
/**
 * @author Cheng.Wei
 */
@Getter
@Setter
@ToString
@Table(sheet = "日志信息", first = 1)
public class Log implements Serializable {
    @Column(name = "A", notes = "账号", typeHandler = StringTypeHandler.class)
    private String username;
    @Column(name = "B", notes = "时间")
    private java.util.Date gmtCreate;
    @Column(name = "C", notes = "操作", typeHandler = StringTypeHandler.class)
    private String type;
    @Column(name = "D", notes = "年龄", typeHandler = IntegerTypeHandler.class)
    private Integer age;
    @Column(name = "E", notes = "价格1", format = "¥#,##0_);(¥#,##0)", typeHandler = DoubleTypeHandler.class)
    private Double price1;
    @Column(name = "F", notes = "价格2", format = "¥#,##0.00_);(¥#,##0.00)",typeHandler = DoubleTypeHandler.class)
    private Double price2;
    @Column(name = "G", notes = "日期", typeHandler = DateTypeHandler.class)
    private java.sql.Date sqlDate;
}
```

```java
public class ExportTest {
    public static void main(String[] args){
        List<Log> logs = new ArrayList<>();
        Log log1 = new Log();
        log1.setUsername("admin");
        log1.setType("新增");
        log1.setGmtCreate(new Date());
        log1.setAge(20);
        log1.setPrice1(20.118D);
        log1.setPrice2(13.1415);
        log1.setSqlDate(new java.sql.Date(System.currentTimeMillis()));
        logs.add(log1);
        Log log2 = new Log();
        log2.setUsername("super");
        log2.setType("修改");
        log2.setGmtCreate(new Date());
        log2.setAge(22);
        log2.setPrice1(22110.228D);
        log2.setPrice2(10.1415);
        log2.setSqlDate(new java.sql.Date(System.currentTimeMillis()+24*60*60*1000));
        logs.add(log2);
        Workbook workbook = ExcelHelper.builder(Log.class).write(logs);
        try( FileOutputStream o = new FileOutputStream("d:\\1234.xlsx")){
            workbook.write(o);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
``` 

### 自定义数据类型转换
继承 tk.jfree.summer.excel.type.AbstractTypeHandler
```java
public class DateTimeTypeHandler extends AbstractTypeHandler<java.util.Date> {


    public DateTimeTypeHandler(Field field) {
        super(field);
    }


    @Override
    public Date get(Cell cell){
      //...自定义取值逻辑
      return null;
    }

    @Override
    public void set(Cell cell, Date value) {
        //...自定义赋值逻辑
    }
}
```
