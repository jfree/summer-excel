/*
 * MIT License
 *
 * Copyright (c) 2019 Cheng.Wei
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package tk.jfree.summer.excel.metadata;

import java.util.List;

/**
 * @author Cheng.Wei
 */
public class Excel {
    /**Excel实体类型*/
    private Class<?> excelType;
    /**sheet名称*/
    private String sheet;
    /**数据起始行*/
    private int first;
    /**映射关系*/
    private List<ExcelField> resultMap;

    public Class<?> getExcelType() {
        return excelType;
    }

    public void setExcelType(Class<?> excelType) {
        this.excelType = excelType;
    }

    public String getSheet() {
        return sheet;
    }

    public void setSheet(String sheet) {
        this.sheet = sheet;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public List<ExcelField> getResultMap() {
        return resultMap;
    }

    public void setResultMap(List<ExcelField> resultMap) {
        this.resultMap = resultMap;
    }

    @Override
    public String toString() {
        return "Excel{" +
                "excelType=" + excelType +
                ", sheet='" + sheet + '\'' +
                ", first=" + first +
                ", resultMap=" + resultMap +
                '}';
    }
}
