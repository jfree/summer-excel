/*
 * MIT License
 *
 * Copyright (c) 2019 Cheng.Wei
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package tk.jfree.summer.excel.type;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormat;
import tk.jfree.summer.excel.util.CellTypeUtil;
import tk.jfree.summer.excel.util.StringUtil;

import java.lang.reflect.Field;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 转换 java.sql.Date
 * @author Cheng.Wei
 */
public class DateTypeHandler extends AbstractTypeHandler<java.sql.Date> {

    public static final String DEFAULT_FORMAT = "yyyy-MM-dd";

    private static final Map<String, DateTimeFormatter> LOCAL_DATE_TIME_FORMATTER =  new ConcurrentHashMap<>();

    public DateTypeHandler(Field field) {
        super(field);
    }

    @Override
    public Date get(Cell cell) {
        if(CellTypeUtil.blank(cell)){
            return null;
        }
        if (CellType.NUMERIC == cell.getCellType()) {
            return new Date(cell.getDateCellValue().getTime());
        }
        String format = Optional.ofNullable(StringUtil.trim(this.format())).orElse(DEFAULT_FORMAT);
        LOCAL_DATE_TIME_FORMATTER.computeIfAbsent(format, DateTimeFormatter::ofPattern);
        LocalDateTime localDateTime = LocalDateTime.parse(cell.getStringCellValue(), LOCAL_DATE_TIME_FORMATTER.get(format));
        Long time = java.util.Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant()).getTime();
        return new Date(time);
    }

    @Override
    public void set(Cell cell, Date value) {
        cell.setCellValue(value);
        DataFormat df = cell.getSheet().getWorkbook().getCreationHelper().createDataFormat();
        CellStyle style = cell.getSheet().getWorkbook().createCellStyle();
        String format = Optional.ofNullable(StringUtil.trim(this.format())).orElse(DEFAULT_FORMAT);
        style.setDataFormat(df.getFormat(format));
        cell.setCellStyle(style);
    }
}
