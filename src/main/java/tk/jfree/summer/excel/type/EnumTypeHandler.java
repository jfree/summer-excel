/*
 * MIT License
 *
 * Copyright (c) 2019 Cheng.Wei
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package tk.jfree.summer.excel.type;

import org.apache.poi.ss.usermodel.Cell;
import tk.jfree.summer.excel.util.CellTypeUtil;

import java.lang.reflect.Field;

/**
 * 普通枚举类转换
 * @author Cheng.Wei
 */
public class EnumTypeHandler extends AbstractTypeHandler<Enum> {

    public EnumTypeHandler(Field field) {
        super(field);
    }

    @Override
    public Enum get(Cell cell){
        return CellTypeUtil.blank(cell) ? null : Enum.valueOf((Class) super.getField().getType(), cell.getStringCellValue());
    }

    @Override
    public void set(Cell cell, Enum value) {
        cell.setCellValue(value.name());
    }
}
