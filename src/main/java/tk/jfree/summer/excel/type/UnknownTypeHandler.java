/*
 * MIT License
 *
 * Copyright (c) 2019 Cheng.Wei
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package tk.jfree.summer.excel.type;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import tk.jfree.summer.excel.ExcelCellValue;

import java.lang.reflect.Field;
import java.util.Optional;

/**
 * 默认情况下不支持特殊格式数据赋值 如：java.util.Date 等，部分参数转换失败
 * 可定制参数转换
 * @see tk.jfree.summer.excel.type.AbstractTypeHandler
 * @see tk.jfree.summer.excel.type.TypeHandler
 * @author Cheng.Wei
 */
public class UnknownTypeHandler extends AbstractTypeHandler<Object> {

    public UnknownTypeHandler(Field field) {
        super(field);
    }

    @Override
    public Object get(Cell cell){
        return CellType.BLANK.equals(cell.getCellType()) ? null : ExcelCellValue.get(cell);
    }

    @Override
    public void set(Cell cell, Object value) {
        TypeHandler typeHandler = new TypeHandlerRegistry(this.getField()).getTypeHandler(this.getField().getType());
        if(Optional.ofNullable(typeHandler).isPresent()){
            typeHandler.set(cell, value);
        }
    }
}
