/*
 * MIT License
 *
 * Copyright (c) 2019 Cheng.Wei
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package tk.jfree.summer.excel.type;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormat;
import tk.jfree.summer.excel.util.CellTypeUtil;
import tk.jfree.summer.excel.util.StringUtil;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 转换 java.time.LocalDate
 * @author Cheng.Wei
 */
public class LocalDateTypeHandler extends AbstractTypeHandler<LocalDate> {

    public LocalDateTypeHandler(Field field) {
       super(field);
    }

    private static final Map<String, DateTimeFormatter> LOCAL_DATE_FORMATTER =  new ConcurrentHashMap<>();

    @Override
    public LocalDate get(Cell cell) {
        if(CellTypeUtil.blank(cell)){
            return null;
        }
        if (CellType.NUMERIC == cell.getCellType()) {
            return LocalDateTime.ofInstant(cell.getDateCellValue().toInstant(), ZoneId.systemDefault()).toLocalDate();
        }
        String format = Optional.ofNullable(StringUtil.trim(this.format())).orElse(DateTypeHandler.DEFAULT_FORMAT);
        LOCAL_DATE_FORMATTER.computeIfAbsent(format, DateTimeFormatter::ofPattern);
        return LocalDate.parse(cell.getStringCellValue(), LOCAL_DATE_FORMATTER.get(format));
    }

    @Override
    public void set(Cell cell, LocalDate value) {
        cell.setCellValue(Date.from(value.atStartOfDay(ZoneId.systemDefault()).toInstant()));
        String format = Optional.ofNullable(StringUtil.trim(this.format())).orElse(DateTypeHandler.DEFAULT_FORMAT);
        DataFormat df = cell.getSheet().getWorkbook().getCreationHelper().createDataFormat();
        CellStyle style = cell.getSheet().getWorkbook().createCellStyle();
        style.setDataFormat(df.getFormat(format));
        cell.setCellStyle(style);

    }
}
