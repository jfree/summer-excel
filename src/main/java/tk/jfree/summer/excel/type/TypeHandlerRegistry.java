/*
 * MIT License
 *
 * Copyright (c) 2019 Cheng.Wei
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package tk.jfree.summer.excel.type;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Cheng.Wei
 */
public final class TypeHandlerRegistry {
    private final Map<Class<?>, TypeHandler<?>> unknownHandlersMap = new HashMap<>();

    public TypeHandlerRegistry(Field field) {
        register(java.lang.Integer.class, new IntegerTypeHandler(field));
        register(java.lang.Long.class, new LongTypeHandler(field));
        register(java.lang.Double.class, new DoubleTypeHandler(field));
        register(java.lang.String.class, new StringTypeHandler(field));
        register(java.time.LocalDate.class, new LocalDateTypeHandler(field));
        register(java.time.LocalDateTime.class, new LocalDateTimeTypeHandler(field));
        register(java.sql.Date.class, new DateTypeHandler(field));
        register(java.util.Date.class, new DateTimeTypeHandler(field));
        register(java.lang.Enum.class, new EnumTypeHandler(field));
    }

    public <T> void register(Class<?> type, TypeHandler<? extends T> typeHandler) {
        unknownHandlersMap.computeIfAbsent(type, k-> typeHandler);
    }

    public TypeHandler getTypeHandler(Class<?> type) {
        return unknownHandlersMap.get(type);
    }

}
