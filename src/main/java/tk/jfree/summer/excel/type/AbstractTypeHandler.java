/*
 * MIT License
 *
 * Copyright (c) 2019 Cheng.Wei
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package tk.jfree.summer.excel.type;

import org.apache.poi.ss.usermodel.Cell;
import tk.jfree.summer.excel.annotation.Column;

import java.lang.reflect.Field;

/**
 * 需定制转换数据只要继承当前类并定制 {@link #get(Cell)}方法来实现
 * 参考：
 * @see IntegerTypeHandler
 * @see DateTypeHandler
 *
 * @author Cheng.Wei
 */
public abstract class AbstractTypeHandler<V> implements TypeHandler<V>{

    private final Field field;

    protected AbstractTypeHandler(Field field) {
        this.field = field;
    }

    public Field getField() {
        return field;
    }

    public String format(){
        return field.getAnnotation(Column.class).format();
    }
}
