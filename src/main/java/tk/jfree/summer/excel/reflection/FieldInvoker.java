/*
 * MIT License
 *
 * Copyright (c) 2019 Cheng.Wei
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package tk.jfree.summer.excel.reflection;

import org.apache.poi.ss.usermodel.Cell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tk.jfree.summer.excel.metadata.ExcelField;
import tk.jfree.summer.excel.util.CellTypeUtil;


/**
 * @author Cheng.Wei
 */
public class FieldInvoker{
    private static final Logger log = LoggerFactory.getLogger(FieldInvoker.class);

    public static void convertToEntityAttribute(Object t, Cell cell, ExcelField excelField) {
        try {
            if(!CellTypeUtil.blank(cell)){
                excelField.getField().set(t, excelField.getTypeHandler().get(cell));
            }
        } catch (Exception e) {
            log.warn("数据读取异常{},{}", excelField.getField(), e.getMessage());
        }
    }

    public static void convertToSheetColumn(Object t, Cell cell, ExcelField excelField) {
        try {
            final Object value = excelField.getField().get(t);
            if(value!=null){
                excelField.getTypeHandler().set(cell, value);
            }
        } catch (Exception e) {
            log.warn("数据写入失败:{}", e.getMessage());
        }
    }
}
