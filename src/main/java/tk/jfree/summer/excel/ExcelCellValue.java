/*
 * MIT License
 *
 * Copyright (c) 2019 Cheng.Wei
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package tk.jfree.summer.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;

/**
 * Excel 单元格取值
 * @author Cheng.Wei
 */
public class ExcelCellValue {
    private ExcelCellValue(){}
    /**
     * 取值
     * @param cell 单元格对象
     * @return 根据单元格类型返回 {@link org.apache.poi.ss.usermodel.CellType}
     */
    public static Object get(Cell cell){
        if (cell == null){
            return null;
        }
        if (cell.getCellType() == CellType.STRING) {
            return cell.getStringCellValue();

        } else if (cell.getCellType() == CellType.BOOLEAN) {
            return cell.getBooleanCellValue();

        } else if (cell.getCellType() == CellType.FORMULA) {
            return cell.getCellFormula();

        } else if (cell.getCellType() == CellType.NUMERIC) {
            return  cell.getNumericCellValue();
        }
        return null;
    }
}
