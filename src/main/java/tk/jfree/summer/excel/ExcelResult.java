/*
 * MIT License
 *
 * Copyright (c) 2019 Cheng.Wei
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package tk.jfree.summer.excel;

import java.util.List;

/**
 * 数据读取结果
 * @author Cheng.Wei
 */
public class ExcelResult<T> {
    /**sheet name*/
    private String sheet;
    /**首行（不含表头）*/
    private Integer firstRow;
    /**最后一行*/
    private Integer lastRow;
    /**读取的数据*/
    private List<T> data;


    public static <T> ExcelResult.ExcelResultBuilder<T> builder() {
        return new ExcelResult.ExcelResultBuilder();
    }

    public String getSheet() {
        return this.sheet;
    }

    public Integer getFirstRow() {
        return this.firstRow;
    }

    public Integer getLastRow() {
        return this.lastRow;
    }

    public List<T> getData() {
        return this.data;
    }

    public void setSheet(String sheet) {
        this.sheet = sheet;
    }

    public void setFirstRow(Integer firstRow) {
        this.firstRow = firstRow;
    }

    public void setLastRow(Integer lastRow) {
        this.lastRow = lastRow;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
    @Override
    public String toString() {
        return "ExcelResult(sheet=" + this.getSheet() + ", firstRow=" + this.getFirstRow() + ", lastRow="
                + this.getLastRow() + ", data=" + this.getData() + ")";
    }

    public ExcelResult() {
    }

    public ExcelResult(String sheet, Integer firstRow, Integer lastRow, List<T> data) {
        this.sheet = sheet;
        this.firstRow = firstRow;
        this.lastRow = lastRow;
        this.data = data;
    }

    public static class ExcelResultBuilder<T> {
        private String sheet;
        private Integer firstRow;
        private Integer lastRow;
        private List<T> data;

        ExcelResultBuilder() {
        }

        public ExcelResult.ExcelResultBuilder<T> sheet(String sheet) {
            this.sheet = sheet;
            return this;
        }

        public ExcelResult.ExcelResultBuilder<T> firstRow(Integer firstRow) {
            this.firstRow = firstRow;
            return this;
        }

        public ExcelResult.ExcelResultBuilder<T> lastRow(Integer lastRow) {
            this.lastRow = lastRow;
            return this;
        }

        public ExcelResult.ExcelResultBuilder<T> data(List<T> data) {
            this.data = data;
            return this;
        }

        public ExcelResult<T> build() {
            return new ExcelResult(this.sheet, this.firstRow, this.lastRow, this.data);
        }

        @Override
        public String toString() {
            return "ExcelResult.ExcelResultBuilder(sheet=" + this.sheet + ", firstRow=" + this.firstRow
                    + ", lastRow=" + this.lastRow + ", data=" + this.data + ")";
        }
    }
}
