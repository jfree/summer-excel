package tk.jfree.summer.excel.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import tk.jfree.summer.excel.annotation.Column;
import tk.jfree.summer.excel.annotation.Table;
import tk.jfree.summer.excel.type.DateTypeHandler;
import tk.jfree.summer.excel.type.DoubleTypeHandler;
import tk.jfree.summer.excel.type.IntegerTypeHandler;
import tk.jfree.summer.excel.type.StringTypeHandler;

import java.io.Serializable;

/**
 * @author Cheng.Wei
 */
@Getter
@Setter
@ToString
@Table(sheet = "日志信息", first = 1)
public class Log implements Serializable {
    @Column(name = "A", notes = "账号", typeHandler = StringTypeHandler.class)
    private String username;
    @Column(name = "B", notes = "时间", width = 20)
    private java.util.Date gmtCreate;
    @Column(name = "C", notes = "操作", typeHandler = StringTypeHandler.class, width = 4)
    private String type;
    @Column(name = "D", notes = "年龄", typeHandler = IntegerTypeHandler.class)
    private Integer age;
    @Column(name = "E", notes = "价格1", format = "¥#,##0_);(¥#,##0)", width = 10, typeHandler = DoubleTypeHandler.class)
    private Double price1;
    @Column(name = "F", notes = "价格2", format = "¥#,##0.00_);(¥#,##0.00)", width = 10, typeHandler = DoubleTypeHandler.class)
    private Double price2;
    @Column(name = "G", notes = "日期", typeHandler = DateTypeHandler.class)
    private java.sql.Date sqlDate;
}
