package tk.jfree.summer.excel;

import org.apache.poi.ss.usermodel.Workbook;
import tk.jfree.summer.excel.bean.Employee;
import tk.jfree.summer.excel.bean.Log;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Cheng.Wei
 */
public class ExportTest {
    public static void main(String[] args) throws Exception {
        List<Log> logs = new ArrayList<>();
        Log log1 = new Log();
        log1.setUsername("admin");
        log1.setType("新增");
        log1.setGmtCreate(new Date());
        log1.setAge(20);
        log1.setPrice1(20.118D);
        log1.setPrice2(13.1415);
        log1.setSqlDate(new java.sql.Date(System.currentTimeMillis()));
        logs.add(log1);
        Log log2 = new Log();
        log2.setUsername("super");
        log2.setType("修改");
        log2.setGmtCreate(new Date());
        log2.setAge(22);
        log2.setPrice1(22110.228D);
        log2.setPrice2(10.1415);
        log2.setSqlDate(new java.sql.Date(System.currentTimeMillis()+24*60*60*1000));
        logs.add(log2);
        Workbook workbook = ExcelHelper.builder(Log.class).write(logs);

        Employee employee = new Employee();
        employee.setName("张三");
        employee.setAge(20);
        employee.setGmtCreate(new Date());
        Employee ls = new Employee();
        ls.setName("李四");
        ls.setAge(30);
        ls.setGmtCreate(new Date());
        List<Employee> emps = new ArrayList<>();
        emps.add(employee);
        emps.add(ls);
        workbook = ExcelHelper.builder(Employee.class).write(emps, workbook);
        try( FileOutputStream o = new FileOutputStream("d:\\1234.xlsx")){
            workbook.write(o);
            workbook.close();
        }catch (Exception e){
            e.printStackTrace();
        }
//        读取
        System.out.println("============读取日志信息==================");
        ExcelHelper.builder(Log.class)
                .read(new FileInputStream("d:\\1234.xlsx"))
                .getData()
                .stream().forEach(System.out::println);
    }
}
